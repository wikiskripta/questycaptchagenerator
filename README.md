# QuestyCaptchaGenerator

This app randomly selects the set of _$questions_count_ questions for QuestyCaptcha from the CSV source.

## Description

* Version 1.0
* Script  _QuestyCaptchaGenerator.php_ randomly selects the set of _$questions_count_ questions for [QuestyCaptcha](https://www.mediawiki.org/wiki/Extension:QuestyCaptcha) from the CSV source.
* Generated set is exported to _QuestyCaptchaDefinition.php_ file in the web root.
* Run with CRON.

## Installation

* Download and place the files to your /extensions/ folder.
* Rename _'data/*.sample'_ files to _'.csv'_ and put your content inside.
* Create CRON job and run _QuestyCaptchaGenerator.php_ every hour.
* Make sure you have [QuestyCaptcha](https://www.mediawiki.org/wiki/Extension:QuestyCaptcha) installed.
* Put `require_once( "$IP/extensions/QuestyCaptchaGenerator/data/QuestyCaptchaDefinition_$wgLanguageCode.php" );` to your _LocalSettings.php_.
* Make sure the /data/ folder is not accessible from the web.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University