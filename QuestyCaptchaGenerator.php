<?php

/**
 * Script randomly selects the set of $questions_count questions for QuestyCaptcha from the CSV source.
 * Run with CRON every hour.
 * @ingroup Apps
 * @author Josef Martiňák
 */

$questions_count = 5; // number of questions in the set
$langs = array('cs', 'en');

foreach( $langs as $lang ) {

	// select $questions_count ramdom questions from the source
	$file = file( __DIR__ . "/data/QuestyCaptchaQuestions_" . $lang . ".csv" );
	$rndlines = array_rand( $file, $questions_count );
	$out = "";
	foreach( $rndlines as $lineNo ) {
		$arr = explode(";", trim($file["$lineNo"]));
		$first = true;
		foreach( $arr as $col ) {
			$col = htmlspecialchars( $col, ENT_QUOTES );
			if( $first ) {
				// question
				$question = "'question' => '$col'";
				$answers = array();
				$first = false;
			}
			else {
				// answer
				if( $col ) array_push( $answers, "'" . $col . "'" );
			}
		}
		$out .= '$wgCaptchaQuestions[] = array( ' . $question . ', \'answer\' => array(' . join( ',', $answers ) . ') )' . ";\n";
	}
	file_put_contents( __DIR__ . "/data/QuestyCaptchaDefinition_" . $lang . ".php", "<?php\n" . $out . "?>" );
}


?>